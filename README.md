# Command Line Discord Bot

!! WARNING !!

Running Command Line on your own Discord account is against the terms of service.

Running Command Line on your own Linux account is bad practice, especially if you have permission to use `sudo`.

I recommend you set up a non-sudo user account to run Command Line that has no permission to read anything but its own Home directory.

This bot was intended for a small community that I trust, so there's little to no safeguards on commands run-able.

!! WARNING END !!

A simple discord bot that allows you to run Linux commands in Discord. Requires the ability to Send and Read messages, but that's it.

Requires [Node.JS](https://nodejs.org/) and [Discord.JS](https://discord.js.org/).


Affectionately known as Cline.